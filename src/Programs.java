public enum Programs {
    PROGRAM1("Java", 850),
    PROGRAM2("PHP", 800),
    PROGRAM3("SQL", 400),
    PROGRAM4("JavaScript", 900),
    PROGRAM5("Automated QA", 800),
    PROGRAM6("Python", 950);

    String nameProgram;
    int durationHrs;

    Programs(String nameProgram, int durationHrs) {
        this.nameProgram = nameProgram;
        this.durationHrs = durationHrs;
    }
}
