import java.util.Scanner;

public class Report {

    public static void main(String[] args){
        Students stud1 = Students.STUDENT1;
        Students stud2 = Students.STUDENT2;
        Students stud3 = Students.STUDENT3;
        Students stud4 = Students.STUDENT4;
        Students stud5 = Students.STUDENT5;
        Students stud6 = Students.STUDENT6;

        System.out.print("Input a number: ");
        Scanner in = new Scanner(System.in);
        int param = in.nextInt();
        if (param == 0) {
            stud1.reportShort();
            stud2.reportShort();
            stud3.reportShort();
            stud4.reportShort();
            stud5.reportShort();
            stud6.reportShort();
        }
        else {
            stud1.reportLong();
            stud2.reportLong();
            stud3.reportLong();
            stud4.reportLong();
            stud5.reportLong();
            stud6.reportLong();
        }
    }
}
