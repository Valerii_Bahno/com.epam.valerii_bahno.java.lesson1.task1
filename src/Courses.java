public enum Courses {
    JAVA("Basic", 350, "Advance", 500),
    PHP("Basic", 300, "Advance", 500),
    SQL("Basic", 200, "Advance", 200),
    JAVASCRIPT("Basic", 250, "Advance", 650),
    AUTOMATED_QA("Basic", 350, "Advance", 450),
    PYTHON("Basic", 300, "Advance", 650);

    String topic1;
    int durationTopic1;
    String topic2;
    int durationTopic2;

    Courses(String topic1, int durationTopic1, String topic2, int durationTopic2) {
        this.topic1 = topic1;
        this.durationTopic1 = durationTopic1;
        this.topic2 = topic2;
        this.durationTopic2 = durationTopic2;
    }
}