import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public enum Students {
    STUDENT1("Ivan Ivanov", "Java", "2020-06-01 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT1.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM1.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT1.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.JAVA.topic1 + "        " + Courses.JAVA.durationTopic1);
            System.out.println(Courses.JAVA.topic2 + "      " + Courses.JAVA.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            /*  Report about Student1   */
            LocalDateTime dateParse1 = LocalDateTime.parse(Students.STUDENT1.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay1 = Duration.between(date, dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8));
            long diffDay1 = Math.abs(durationDay1.toDays());
            Duration durationHour1 = Duration.between(date, dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8));
            double diffHour1 = Math.abs(diffDay1 - (Math.abs(durationHour1.toHours()/24.0)))*24;

            if (date.compareTo(dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT1.nameStudent + " ("+ Students.STUDENT1.programStudent +")" + studyEnd + diffDay1 + " д " + (int)diffHour1 + " ч");
            }
            if (date.compareTo(dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT1.nameStudent + " ("+ Students.STUDENT1.programStudent +")" + studyGo + diffDay1 + " д " + (int)diffHour1 + " ч");
            }
        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student1  */
            LocalDateTime dateParse1 = LocalDateTime.parse(Students.STUDENT1.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            /*  Full report about Student1  */
            System.out.println("\nFIO: " + Students.STUDENT1.nameStudent);
            Duration durationDaySpent1 = Duration.between(date, dateParse1);
            long diffDaySpent1 = Math.abs(durationDaySpent1.toDays());
            Duration durationHourSpent1 = Duration.between(date, dateParse1);
            double diffHourSpent1 = Math.abs(diffDaySpent1 - (Math.abs(durationHourSpent1.toHours()/24.0)))*24;
            if (date.compareTo(dateParse1) > 0) {
                System.out.println("Time spent: " + (diffDaySpent1*8 + (int)diffHourSpent1) + "h");
            }
            if ((date.compareTo(dateParse1) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT1.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM1.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT1.dateStart);
            System.out.println("END_DATE: " + dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8));
            Duration durationDayEnd1 = Duration.between(date, dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8));
            long diffDayEnd1 = Math.abs(durationDayEnd1.toDays());
            Duration durationHourEnd1 = Duration.between(date, dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8));
            double diffHourEnd1 = Math.abs(diffDayEnd1 - (Math.abs(durationHourEnd1.toHours()/24.0)))*24;
            if (date.compareTo(dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd1*8 + "d "  + (int)diffHourEnd1 + "h");
            }
            if (date.compareTo(dateParse1.plusDays((Programs.PROGRAM1.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd1*8 + "d "  + (int)diffHourEnd1 + "h");
            }

        }
    },
    STUDENT2("Petr Petrov", "PHP", "2020-06-10 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT2.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM2.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT2.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.PHP.topic1 + "        " + Courses.PHP.durationTopic1);
            System.out.println(Courses.PHP.topic2 + "      " + Courses.PHP.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            /*  DateStart for Student1  */
            LocalDateTime dateParse1 = LocalDateTime.parse(Students.STUDENT1.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            LocalDateTime dateParse2 = LocalDateTime.parse(Students.STUDENT2.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay2 = Duration.between(date, dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8));
            long diffDay2 = Math.abs(durationDay2.toDays());
            Duration durationHour2 = Duration.between(date, dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8));
            double diffHour2 = Math.abs(diffDay2 - (Math.abs(durationHour2.toHours()/24.0)))*24;

            if (date.compareTo(dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT2.nameStudent + " ("+ Students.STUDENT2.programStudent +")" + studyEnd + diffDay2 + " д " + (int)diffHour2 + " ч");
            }
            if (date.compareTo(dateParse1.plusDays((Programs.PROGRAM2.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT2.nameStudent + " ("+ Students.STUDENT2.programStudent +")" + studyGo + diffDay2 + " д " + (int)diffHour2 + " ч");
            }

        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student2  */
            LocalDateTime dateParse2 = LocalDateTime.parse(Students.STUDENT2.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            System.out.println("\nFIO: " + Students.STUDENT2.nameStudent);
            Duration durationDaySpent2 = Duration.between(date, dateParse2);
            long diffDaySpent2 = Math.abs(durationDaySpent2.toDays());
            Duration durationHourSpent2 = Duration.between(date, dateParse2);
            double diffHourSpent2 = Math.abs(diffDaySpent2 - (Math.abs(durationHourSpent2.toHours()/24.0)))*24;
            if (date.compareTo(dateParse2) > 0) {
                System.out.println("Time spent: " + (diffDaySpent2*8 + (int)diffHourSpent2) + "h");
            }
            if ((date.compareTo(dateParse2) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT2.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM2.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT2.dateStart);
            System.out.println("END_DATE: " + dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8));
            Duration durationDayEnd2 = Duration.between(date, dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8));
            long diffDayEnd2 = Math.abs(durationDayEnd2.toDays());
            Duration durationHourEnd2 = Duration.between(date, dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8));
            double diffHourEnd2 = Math.abs(diffDayEnd2 - (Math.abs(durationHourEnd2.toHours()/24.0)))*24;
            if (date.compareTo(dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd2*8 + "d "  + (int)diffHourEnd2 + "h");
            }
            if (date.compareTo(dateParse2.plusDays((Programs.PROGRAM2.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd2*8 + "d "  + (int)diffHourEnd2 + "h");
            }
        }
    },
    STUDENT3("Sidor Sidorov", "SQL", "2020-06-15 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT3.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM3.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT3.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.SQL.topic1 + "        " + Courses.SQL.durationTopic1);
            System.out.println(Courses.SQL.topic2 + "      " + Courses.SQL.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            LocalDateTime dateParse3 = LocalDateTime.parse(Students.STUDENT3.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay3 = Duration.between(date, dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8));
            long diffDay3 = Math.abs(durationDay3.toDays());
            Duration durationHour3 = Duration.between(date, dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8));
            double diffHour3 = Math.abs(diffDay3 - (Math.abs(durationHour3.toHours()/24.0)))*24;

            if (date.compareTo(dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT3.nameStudent + " ("+ Students.STUDENT3.programStudent +")" + studyEnd + diffDay3 + " д " + (int)diffHour3 + " ч");
            }
            if (date.compareTo(dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT3.nameStudent + " ("+ Students.STUDENT3.programStudent +")" + studyGo + diffDay3 + " д " + (int)diffHour3 + " ч");
            }
        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student3  */
            LocalDateTime dateParse3 = LocalDateTime.parse(Students.STUDENT3.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            System.out.println("\nFIO: " + Students.STUDENT3.nameStudent);
            Duration durationDaySpent3 = Duration.between(date, dateParse3);
            long diffDaySpent3 = Math.abs(durationDaySpent3.toDays());
            Duration durationHourSpent3 = Duration.between(date, dateParse3);
            double diffHourSpent3 = Math.abs(diffDaySpent3 - (Math.abs(durationHourSpent3.toHours()/24.0)))*24;
            if (date.compareTo(dateParse3) > 0) {
                System.out.println("Time spent: " + (diffDaySpent3*8 + (int)diffHourSpent3) + "h");
            }
            if ((date.compareTo(dateParse3) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT3.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM3.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT3.dateStart);
            System.out.println("END_DATE: " + dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8));
            Duration durationDayEnd3 = Duration.between(date, dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8));
            long diffDayEnd3 = Math.abs(durationDayEnd3.toDays());
            Duration durationHourEnd3 = Duration.between(date, dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8));
            double diffHourEnd3 = Math.abs(diffDayEnd3 - (Math.abs(durationHourEnd3.toHours()/24.0)))*24;
            if (date.compareTo(dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd3*8 + "d "  + (int)diffHourEnd3 + "h");
            }
            if (date.compareTo(dateParse3.plusDays((Programs.PROGRAM3.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd3*8 + "d "  + (int)diffHourEnd3 + "h");
            }
        }
    },
    STUDENT4("Max Maximov", "JavaScript", "2020-07-10 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT4.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM4.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT4.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.JAVASCRIPT.topic1 + "        " + Courses.JAVASCRIPT.durationTopic1);
            System.out.println(Courses.JAVASCRIPT.topic2 + "      " + Courses.JAVASCRIPT.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            LocalDateTime dateParse4 = LocalDateTime.parse(Students.STUDENT4.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay4 = Duration.between(date, dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8));
            long diffDay4 = Math.abs(durationDay4.toDays());
            Duration durationHour4 = Duration.between(date, dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8));
            double diffHour4 = Math.abs(diffDay4 - (Math.abs(durationHour4.toHours()/24.0)))*24;

            if (date.compareTo(dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT4.nameStudent + " ("+ Students.STUDENT4.programStudent +")" + studyEnd + diffDay4 + " д " + (int)diffHour4 + " ч");
            }
            if (date.compareTo(dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT4.nameStudent + " ("+ Students.STUDENT4.programStudent +")" + studyGo + diffDay4 + " д " + (int)diffHour4 + " ч");
            }
        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student4  */
            LocalDateTime dateParse4 = LocalDateTime.parse(Students.STUDENT4.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            System.out.println("\nFIO: " + Students.STUDENT4.nameStudent);
            Duration durationDaySpent4 = Duration.between(date, dateParse4);
            long diffDaySpent4 = Math.abs(durationDaySpent4.toDays());
            Duration durationHourSpent4 = Duration.between(date, dateParse4);
            double diffHourSpent4 = Math.abs(diffDaySpent4 - (Math.abs(durationHourSpent4.toHours()/24.0)))*24;
            if (date.compareTo(dateParse4) > 0) {
                System.out.println("Time spent: " + (diffDaySpent4*8 + (int)diffHourSpent4) + "h");
            }
            if ((date.compareTo(dateParse4) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT4.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM4.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT4.dateStart);
            System.out.println("END_DATE: " + dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8));
            Duration durationDayEnd4 = Duration.between(date, dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8));
            long diffDayEnd4 = Math.abs(durationDayEnd4.toDays());
            Duration durationHourEnd4 = Duration.between(date, dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8));
            double diffHourEnd4 = Math.abs(diffDayEnd4 - (Math.abs(durationHourEnd4.toHours()/24.0)))*24;
            if (date.compareTo(dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd4*8 + "d "  + (int)diffHourEnd4 + "h");
            }
            if (date.compareTo(dateParse4.plusDays((Programs.PROGRAM4.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd4*8 + "d "  + (int)diffHourEnd4 + "h");
            }
        }
    },
    STUDENT5("Alex Alexov", "Automated QA", "2020-08-01 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT5.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM5.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT5.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.AUTOMATED_QA.topic1 + "        " + Courses.AUTOMATED_QA.durationTopic1);
            System.out.println(Courses.AUTOMATED_QA.topic2 + "      " + Courses.AUTOMATED_QA.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            LocalDateTime dateParse5 = LocalDateTime.parse(Students.STUDENT5.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay5 = Duration.between(date, dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8));
            long diffDay5 = Math.abs(durationDay5.toDays());
            Duration durationHour5 = Duration.between(date, dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8));
            double diffHour5 = Math.abs(diffDay5 - (Math.abs(durationHour5.toHours()/24.0)))*24;

            if (date.compareTo(dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT5.nameStudent + " ("+ Students.STUDENT5.programStudent +")" + studyEnd + diffDay5 + " д " + (int)diffHour5 + " ч");
            }
            if (date.compareTo(dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT5.nameStudent + " ("+ Students.STUDENT5.programStudent +")" + studyGo + diffDay5 + " д " + (int)diffHour5 + " ч");
            }
        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student5  */
            LocalDateTime dateParse5 = LocalDateTime.parse(Students.STUDENT5.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            System.out.println("\nFIO: " + Students.STUDENT5.nameStudent);
            Duration durationDaySpent5 = Duration.between(date, dateParse5);
            long diffDaySpent5 = Math.abs(durationDaySpent5.toDays());
            Duration durationHourSpent5 = Duration.between(date, dateParse5);
            double diffHourSpent5 = Math.abs(diffDaySpent5 - (Math.abs(durationHourSpent5.toHours()/24.0)))*24;
            if (date.compareTo(dateParse5) > 0) {
                System.out.println("Time spent: " + (diffDaySpent5*8 + (int)diffHourSpent5) + "h");
            }
            if ((date.compareTo(dateParse5) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT5.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM5.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT5.dateStart);
            System.out.println("END_DATE: " + dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8));
            Duration durationDayEnd5 = Duration.between(date, dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8));
            long diffDayEnd5 = Math.abs(durationDayEnd5.toDays());
            Duration durationHourEnd5 = Duration.between(date, dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8));
            double diffHourEnd5 = Math.abs(diffDayEnd5 - (Math.abs(durationHourEnd5.toHours()/24.0)))*24;
            if (date.compareTo(dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd5*8 + "d "  + (int)diffHourEnd5 + "h");
            }
            if (date.compareTo(dateParse5.plusDays((Programs.PROGRAM5.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd5*8 + "d "  + (int)diffHourEnd5 + "h");
            }
        }
    },
    STUDENT6("Boris Borisov", "Python", "2020-08-10 10:00"){
        @Override
        void reportStudent() {
            System.out.println("STUDENT: " + Students.STUDENT6.nameStudent);
            System.out.println("CURRICULUM: " + Programs.PROGRAM6.nameProgram);
            System.out.println("START_DATE: " + Students.STUDENT6.dateStart);
            System.out.println("COURSE  " + "DURATION (hrs)");
            System.out.println(Courses.PYTHON.topic1 + "        " + Courses.PYTHON.durationTopic1);
            System.out.println(Courses.PYTHON.topic2 + "      " + Courses.PYTHON.durationTopic2);
        }

        @Override
        void reportShort() {
            LocalDateTime date = LocalDateTime.now();
            String studyEnd = " - Обучение закончено. После окончания прошло ";
            String studyGo = " - Обучение не закончено. До окончания осталось ";

            LocalDateTime dateParse6 = LocalDateTime.parse(Students.STUDENT6.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            Duration durationDay6 = Duration.between(date, dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8));
            long diffDay6 = Math.abs(durationDay6.toDays());
            Duration durationHour6 = Duration.between(date, dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8));
            double diffHour6 = Math.abs(diffDay6 - (Math.abs(durationHour6.toHours()/24.0)))*24;

            if (date.compareTo(dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8)) > 0) {
                System.out.println(Students.STUDENT6.nameStudent + " ("+ Students.STUDENT6.programStudent +")" + studyEnd + diffDay6 + " д " + (int)diffHour6 + " ч");
            }
            if (date.compareTo(dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8)) < 0) {
                System.out.println(Students.STUDENT6.nameStudent + " ("+ Students.STUDENT6.programStudent +")" + studyGo + diffDay6 + " д " + (int)diffHour6 + " ч");
            }
        }

        @Override
        void reportLong() {
            LocalDateTime date = LocalDateTime.now();

            /*  DateStart for Student6  */
            LocalDateTime dateParse6 = LocalDateTime.parse(Students.STUDENT6.dateStart, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            System.out.println("\nFIO: " + Students.STUDENT6.nameStudent);
            Duration durationDaySpent6 = Duration.between(date, dateParse6);
            long diffDaySpent6 = Math.abs(durationDaySpent6.toDays());
            Duration durationHourSpent6 = Duration.between(date, dateParse6);
            double diffHourSpent6 = Math.abs(diffDaySpent6 - (Math.abs(durationHourSpent6.toHours()/24.0)))*24;
            if (date.compareTo(dateParse6) > 0) {
                System.out.println("Time spent: " + (diffDaySpent6*8 + (int)diffHourSpent6) + "h");
            }
            if ((date.compareTo(dateParse6) < 0)) {
                System.out.println("Time spent: " + "0h");
            }
            System.out.println("CURRICULUM: " + Students.STUDENT6.programStudent);
            System.out.println("Course duration: " + Programs.PROGRAM6.durationHrs + "h");
            System.out.println("START_DATE: " + Students.STUDENT6.dateStart);
            System.out.println("END_DATE: " + dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8));
            Duration durationDayEnd6 = Duration.between(date, dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8));
            long diffDayEnd6 = Math.abs(durationDayEnd6.toDays());
            Duration durationHourEnd6 = Duration.between(date, dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8));
            double diffHourEnd6 = Math.abs(diffDayEnd6 - (Math.abs(durationHourEnd6.toHours()/24.0)))*24;
            if (date.compareTo(dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8)) > 0) {
                System.out.println("Time after course finishing: " + diffDayEnd6*8 + "d "  + (int)diffHourEnd6 + "h");
            }
            if (date.compareTo(dateParse6.plusDays((Programs.PROGRAM6.durationHrs)/8)) < 0) {
                System.out.println("Time left: " + diffDayEnd6*8 + "d "  + (int)diffHourEnd6 + "h");
            }
        }
    };

    String nameStudent;
    String programStudent;
    String dateStart;

    Students(String nameStudent, String programStudent, String dateStart) {
        this.nameStudent = nameStudent;
        this.programStudent = programStudent;
        this.dateStart = dateStart;
    }

    abstract void reportStudent();
    abstract void reportShort();
    abstract void reportLong();
}
